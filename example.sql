begin;

-- create schema
create schema if not exists export;
drop materialized view if exists export.artsregistrering_2023;
create materialized view export.artsregistrering_2023 as
with
    sections as (
        select
            id,
            form::jsonb->'forms' as forms
        from
            notes
        where
            form::jsonb->>'sectionname'= 'artskartlegging'
    ),
     items as (
        select
            id,
            jsonb_path_query(forms, '$[*]?(@."formname" == "artsregistrering")."formitems"'::jsonpath) as specie,
            jsonb_path_query(forms, '$[*]?(@."formname" == "kommentar/bilde")."formitems"'::jsonpath) as extra
        from
            sections
     ),
     data as (
        select
            id,
            jsonb_path_query(specie, '$[*]?(@."key" == "art")'::jsonpath) ->> 'value' as art,
            jsonb_path_query(specie, '$[*]?(@."key" == "egendefinert art")'::jsonpath) ->> 'value' as egendefinert_art,
            jsonb_path_query(specie, '$[*]?(@."key" == "habitat")'::jsonpath) ->> 'value' as habitat,
            jsonb_path_query(specie, '$[*]?(@."key" == "egendefinert habitat")'::jsonpath) ->> 'value' as egendefinert_habitat,
            jsonb_path_query(specie, '$[*]?(@."key" == "substrate")'::jsonpath) ->> 'value' as substrat,
            jsonb_path_query(specie, '$[*]?(@."key" == "egendefinert substrat")'::jsonpath) ->> 'value' as egendefinert_substrat,
            jsonb_path_query(specie, '$[*]?(@."key" == "økologi")'::jsonpath) ->> 'value' as økologi,
            jsonb_path_query(specie, '$[*]?(@."key" == "antall")'::jsonpath) ->> 'value' as antall,
            jsonb_path_query(specie, '$[*]?(@."key" == "observasjonstype")'::jsonpath) ->> 'value' as observasjonstype,
            jsonb_path_query(specie, '$[*]?(@."key" == "kartlegger")'::jsonpath) ->> 'value' as kartlegger,
            jsonb_path_query(extra, '$[*]?(@."key" == "kommentar")'::jsonpath) ->> 'value' as kommentar,
            string_to_array(jsonb_path_query(extra, '$[*]?(@."key" == "bilde")'::jsonpath) ->> 'value', ';') as bilder
        from
            items
     ),
     complete as (
        select
            data.id,
            notes.the_geom as geom,
            st_x(notes.the_geom) as longitude,
            st_y(notes.the_geom) as latitude,
            notes.accuracy,
            timezone('UTC'::text, to_timestamp((notes.ts/1000)::double precision)) as creation_time,
            gpapproject.name as project,
            data.art,
            data.egendefinert_art,
            data.habitat,
            data.egendefinert_habitat,
            data.substrat,
            data.egendefinert_substrat,
            data.økologi,
            data.antall,
            data.observasjonstype,
            data.kartlegger,
            data.kommentar,
            data.bilder
        from
            notes
        join
            data
            on data.id = notes.id
        join
            gpapproject
            on gpapproject.id = notes.gpapprojectid
    )
select
    *
from
    complete
where
    date_part('year', creation_time)::int = 2023
;

-- allow refresh materialized view *concurrently*
create unique index on export.artsregistrering_2023 (id);

-- performance improvement
create index on export.artsregistrering_2023 using gist (geom);

-- autorefresh
create or replace function refresh_export_artsregistrering_2023()
returns trigger language plpgsql
as $$
begin
    refresh materialized view concurrently export.artsregistrering_2023;
    return null;
end $$;
create trigger refresh_export_artsregistrering_2023 -- or replace, from Postgresql 14
    after insert or update or delete or truncate
    on notes for each statement
    execute procedure refresh_export_artsregistrering_2023();

commit;
