#!/usr/bin/env python3

import argparse

import psycopg
import pyexiv2


def dd2dms(coordinate):
    d = round(coordinate)
    m = round(60 * abs(coordinate - d))
    s = 3600 * abs(coordinate - d) - 60 * m
    return d, m, s


def dd2exif(coordinate):
    d, m, s = dd2dms(coordinate)
    return f"{abs(d):.0f}/1 {m:.0f}/1 {s*100:.0f}/100"


def extract(conninfo):
    with psycopg.connect(conninfo) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                with images as (
                        select
                            unnest(string_to_array("kommentar/bilde_bilde", ';'))::bigint as id,
                        latitude,
                        longitude
                        from
                            export.artsregistrering_2024
                    )
                select
                    images.id,
                    imagedata.data,
                    images.latitude,
                    images.longitude
                from
                    images
                left join
                    imagedata on imagedata.id = images.id
            """
            )
            for record in cur:
                id, data, latitude, longitude = record
                image_path = f"images/{id}.jpg"
                with open(image_path, "wb") as image_fp:
                    image_fp.write(data)
                image = pyexiv2.Image(image_path)
                image.modify_exif(
                    {
                        "Exif.GPSInfo.GPSLatitude": dd2exif(latitude),
                        "Exif.GPSInfo.GPSLatitudeRef": "N" if latitude > 0 else "S",
                        "Exif.GPSInfo.GPSLongitude": dd2exif(longitude),
                        "Exif.GPSInfo.GPSLongitudeRef": "E" if longitude > 0 else "W",
                    }
                )
                image.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("conninfo")
    args = parser.parse_args()
    extract(args.conninfo)
