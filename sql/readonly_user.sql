create role nina_ansatte login password 'SECRET_PASSWORD';
grant usage on schema export to nina_ansatte;
grant select on all tables in schema export to nina_ansatte;
